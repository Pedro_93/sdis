package SSL.client;

import SSL.server.SSLServer;

import javax.net.ssl.SSLSocket;
import java.io.IOException;
import java.util.ArrayList;

/**
 * Created by Pedro on 08-04-2014.
 */
/*
java SSLClient <host> <port> <oper> <opnd>* <cypher-suite>*

<host>
    is the DNS name (or the IP address, in the dotted decimal format) where the server is running
<port>
    is the port number where the server is providing service
<oper>
    is the operation to request from the server
<opnd>*
    is the list of operands of that operation
<cypher-suite>*
    Is a sequence, possibly empty, of strings specifying the combination of cryptographic algorithms
    the client should use, in order of preference. If no cypher suite is specified, the client shall
    use any of the cypher-suites negotiated by omission by the SSL provider of JSE.
*/
public class SSLClient {

    private static String host= null;
    private static String oper=null;
    private static ArrayList<String> opnd=null;
    private static ArrayList<String> cypher=null;

    public static void main(String[] args) throws IOException {


    }

    private static void writeMessage(String result)
    {
        System.out.println("SSLClient: "+oper.toString()+" "+ opnd.toString()+" :" +result);
    }
}
