/* java Client <host> <remote_object_name> <oper> <opnd>*
where

<host>
is the host name, or IP address, where the server executes
<remote_object_name>
is the name the server bound the remote object to
<oper>
is the server operation to invoke
<opnd>*
is the list of operands for the corresponding operation
The client shall submit the request and receive the response.
It shall also print on its standard output messages that reveal the actions it is performing.
The client shall terminate after receiving the response and printing the corresponding messages.
*/

package RMI.client;

import RMI.RemoteObject;

import java.rmi.registry.Registry;
import java.rmi.registry.LocateRegistry;

public class Client {

    private Client() {}

    public static void main(String[] args) {

        String host = (args.length < 1) ? null : args[0];
        try {
            Registry registry = LocateRegistry.getRegistry(host);
            RemoteObject stub = (RemoteObject) registry.lookup("RemoteObject");
            String response = stub.sayHello();
            System.out.println("Response: " + response);
        } catch (Exception e) {
            System.err.println("Client exception: " + e.toString());
            e.printStackTrace();
        }
    }

}