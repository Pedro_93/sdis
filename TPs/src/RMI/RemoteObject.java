package RMI;

import java.rmi.Remote;
import java.rmi.RemoteException;

public interface RemoteObject extends Remote {
    String sayHello() throws RemoteException;
}