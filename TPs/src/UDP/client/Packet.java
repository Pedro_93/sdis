package UDP.client;

import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.net.SocketException;

public class Packet{

	private static DatagramSocket socket=null;
	private static InetAddress address=null;
	private static boolean success;

    public Packet(DatagramSocket socket,InetAddress address)
	{
        Packet.socket=socket;
		Packet.address=address;
	}

    public String send(String msg)
    {
        Packet.success=false;
        while(!Packet.success)
        {
            byte buf[] = msg.getBytes();
            DatagramPacket packet = new DatagramPacket(buf, buf.length,address,4445);
            try {
                socket.send(packet);

                System.out.println("Client requests: "+msg);

                socket.setSoTimeout(3000);

                //reading response
                buf = new byte[1024];
                packet = new DatagramPacket(buf, buf.length);
                try {
                    socket.receive(packet);
                } catch (IOException e) {
                    return "Error receiving packet through socket..."+e.toString();
                }
                Packet.success=true;
                String received = new String(packet.getData(), 0, packet.getLength());
                return received;

            } catch (SocketException i) {
                return "Connection timed out, no ACK, error sending packet"+i.toString();
            } catch (IOException e) {
                return "Error sending packet through socket..."+e.toString();
            }
        }
        return "";
    }

}
