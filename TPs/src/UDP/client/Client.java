package UDP.client;

/**
 * Created by Pedro on 11-03-2014.
 * UDP based client
 */

import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.net.MulticastSocket;

public class Client {

    static DatagramPacket packet = null;
    static MulticastSocket socket = null;
    static InetAddress address = null;
    static Packet pack = null;

    public static void main(String[] args) {
        if(args.length<4 && (!args[2].equals("register") || !args[2].equals("lookup")))
        {
            System.out.println("Invalid parameters");
            System.out.println("Usage: java client <mcast_addr> <mcast_port> <oper> <opnd>* ");
            System.exit(-1);
        }

          /*
            Variable values for testing purposes
            @mcast_addr - 225.32.46.98
            @mcast_port - 4446
            @oper - REGISTER
            @opnd - 30-23-56 RUI
         */

        System.out.println("Args IP:"+args[0]+"\n\tPORT: "+args[1]+"\n\tOPER: "+args[2]+"\n\tOPND: "+args[3]);

        String mcast_addr,oper,opnd;
        mcast_addr=args[0];
        int mcast_port=Integer.parseInt(args[1]);
        oper=args[2];
        opnd="";
        for(int i=3; i<args.length;i++)
        {
            opnd+=args[i];
            opnd+=" ";
        }
        opnd = opnd.substring(0, Math.min(opnd.length(),opnd.length()-1)); //remove o espaco no fim do argumento

        try {
            socket = new MulticastSocket(mcast_port);
            System.out.print("Attempting to connect to multicast host: " + mcast_addr + "...");
            address = InetAddress.getByName(mcast_addr);
            System.out.println("success!");
        } catch (Exception e) {
            System.out.println("oh no :( \n\t "+e.toString());
            socket.close();
            return;
        }

        try {
            System.out.print("Attempting to join multicast group...");
            socket.joinGroup(address);
            System.out.println("success!");
        }catch(Exception e){
            System.out.println("Error "+e.toString()+"\n\t aka: You've got too much swag to join!");
            e.printStackTrace();
            socket.close();
            System.exit(-1);
        }

        //Get IP and Port for singlecast comunication
        byte buf[] = new byte[1024];
        packet = new DatagramPacket(buf, buf.length);
        try {
            System.out.println("Receiving IP and Port for singlecast communication");
            socket.receive(packet);
        } catch (IOException e) {
            e.printStackTrace();
            System.out.println("Failed to receive information from multicastsocket server "+e.toString());
            socket.close();
            System.exit(-1);
        }

        String received = new String(packet.getData(), 0, packet.getLength());
        String[] parts = received.split("/");
        String IP = parts[0];
        String port = parts[1];

        //Disconnect from multicast group
        try {
            System.out.println("Disconnecting from group");
            socket.leaveGroup(address);
        } catch (Exception e) {
            System.out.println("Error leaving group "+e.toString());
            e.printStackTrace();
            socket.close();
            System.exit(-1);
        }
        socket.close();

        // uniCast connection

        try {
            System.out.println("Address: " + IP);
            System.out.println("Port: " + port);
            DatagramSocket sock = new DatagramSocket();

            System.out.println("Attempting to connect to unicast server");

            address = InetAddress.getByName(IP);
            pack = new Packet(sock,address);
            System.out.println(pack.send(oper + " " + opnd));
            sock.close();
        } catch (Exception e) {
            System.out.println("Unable to connect to host |" + IP + "| err->" + e.toString());
            return;
        }
    }
}
