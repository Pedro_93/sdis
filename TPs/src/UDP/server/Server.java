package UDP.server;

import UDP.server.Database;

import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;

public class Server {
    public static void main(String[] args) throws IOException{

        if(args.length != 3){
            System.out.println("Wrong Arguments!");
            System.out.println("Usage: java Server <srvc_port> <mcast_addr> <mcast_port>");
            System.exit(-1);
        }

        /*
            Variable values for testing purposes
            @srvcPort - 4445
            @mcastAddr - 225.32.46.98
            @mcastPort - 4446
         */

        InetAddress srvcAddr = InetAddress.getLocalHost();
        int srvcPort = Integer.parseInt(args[0]);

        InetAddress mcastAddr = InetAddress.getByName(args[1]);
        int mcastPort = Integer.parseInt(args[2]);

        DatagramSocket S = new DatagramSocket(srvcPort);

        System.out.println("Creating database...");
        Database data = new Database();

        System.out.println("Booting server...");
        System.out.println("External IP: " + srvcAddr.getHostAddress());

        MulticastService mcastThread = new MulticastService("Multicast Announcement", mcastPort, mcastAddr, srvcPort, srvcAddr);
        mcastThread.start();

        while(true){
            byte[] info = new byte[1024];
            // Receber informacao
            DatagramPacket received = new DatagramPacket(info, info.length);
            S.receive(received);
            String receivedS = new String(received.getData()).trim();
            System.out.println("Received: "+receivedS);

            // Obter informacao
            InetAddress ip = received.getAddress();
            int port = received.getPort();

            // REGEX //
            // LOOKUP - (?i)^lookup [a-z0-9]{2}-[a-z0-9]{2}-[a-z0-9]{2}$
            // REGISTER - (?i)^register [a-z0-9]{2}-[a-z0-9]{2}-[a-z0-9]{2} [a-z]+$
            // EXIT - (?i)^exit$

            if( receivedS.matches("(?i)^lookup [a-z0-9]{2}-[a-z0-9]{2}-[a-z0-9]{2}$") ){                      // LOOKUP
                String plateNo = new String( receivedS.substring("LOOKUP ".length()));
                String answerS = new String( plateNo.trim() + " " + data.lookup(plateNo.trim()) );
                DatagramPacket answer = new DatagramPacket(answerS.getBytes(), answerS.length(), ip, port);
                S.send(answer);
            } else if( receivedS.matches("(?i)^register [a-z0-9]{2}-[a-z0-9]{2}-[a-z0-9]{2} [a-z]+$") ){      // REGISTER
                String plateNo = new String( receivedS.substring( "REGISTER ".length(), "REGISTER ".length()+8));
                String owner = new String( receivedS.substring( "REGISTER ".length()+9) );
                int answerS = data.register( plateNo.trim(), owner.trim() );
                DatagramPacket answer = new DatagramPacket(Integer.toString(answerS).getBytes(), Integer.toString(answerS).length(), ip, port);
                S.send(answer);
            } else if( receivedS.matches("(?i)^exit$") ){                                                     // END COMUNICATIONS
                DatagramPacket answer = new DatagramPacket("ACK".getBytes(), "ACK".length(), ip, port);
                S.send(answer);
                break;
            } else {                                                                                          // WHEN ALL ELSE FAILS
                DatagramPacket answer = new DatagramPacket("PLEASE TALK TO ME".getBytes(), "PLEASE TALK TO ME".length(), ip, port);
                S.send(answer);
            }
        }

        mcastThread.close();
        S.close();
        System.out.println("Shutting down server...");
    }
}
