package UDP.server;

import java.io.IOException;
import java.net.DatagramPacket;
import java.net.InetAddress;
import java.net.MulticastSocket;
import java.net.UnknownHostException;

public class MulticastService extends Thread {

    private MulticastSocket S;
    private int mcastPort, srvcPort;
    private InetAddress mcastAddr, srvcAddr;

    public MulticastService(String name, int mcastPort, InetAddress mcastAddr, int srvcPort, InetAddress srvcAddr){
        super(name);
        this.mcastPort = mcastPort;
        this.mcastAddr = mcastAddr;
        this.srvcAddr = srvcAddr;
        this.srvcPort = srvcPort;
    }

    public void run() {

        try {
            S = new MulticastSocket(mcastPort);
            S.setTimeToLive(1);
        } catch (IOException IOE) {
            System.out.println("Creating a multicast socket made an IOException!");
        }

        String buf = new String(srvcAddr.getHostAddress() + "/" + String.valueOf(srvcPort));

        while(true){
            try {
                InetAddress group = InetAddress.getByName(mcastAddr.getHostAddress());
            } catch (UnknownHostException UHE) {
                System.out.println("Unknown Host: " + mcastAddr);
            }

            try{
                S.send(new DatagramPacket(buf.getBytes(), buf.length(), mcastAddr, mcastPort));
                //System.out.println(buf);
                sleep(1000);
            } catch (Exception E){
                System.out.println("An error as ocurred while sending the packet!");
            }
        }
    }

    public void close(){
        S.close();
    }
}
