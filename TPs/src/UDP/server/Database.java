package UDP.server;

import java.util.HashMap;

public class Database {
    private HashMap<String, String> db;

    public Database(){
        this.db = new HashMap<String, String>();
    }

    public int register( String plate, String owner ){
        if( this.db.get(plate) != null )
            return -1;
        else
            this.db.put(plate, owner);

        return this.db.size();
    }

    public String lookup( String plate ){
        String result = this.db.get(plate);
        if( result == null )
            return "ERROR";
        else
            return result;
    }
}
