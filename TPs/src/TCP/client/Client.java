package TCP.Client;

/**
 * Created by Pedro on 11-03-2014.
 * TCP based client
 */

import java.io.BufferedReader;
import java.io.PrintWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.InetAddress;
import java.net.Socket;

public class Client {

    static Socket echoSock=null;

    public static void main(String[] args) {

        if(args.length<4 && (!args[2].equals("register") || !args[2].equals("lookup")))
        {
            System.out.println("Invalid parameters");
            System.out.println("Usage: Java client <svc_name> <svc_port> <oper> <opnd> *");
            System.exit(-1);
        }

        /*
            Variable values for testing purposes
            @mcast_addr - 225.32.46.98
            @mcast_port - 4446
            @oper - REGISTER
            @opnd - 30-23-56 RUI
        */

        System.out.println("Args IP:"+args[0]+"\n\tPORT: "+args[1]+"\n\tOPER: "+args[2]+"\n\tOPND: "+args[3]);

        String mcast_addr,oper,opnd;
        mcast_addr=args[0];
        int mcast_port=Integer.parseInt(args[1]);
        oper=args[2];
        opnd="";
        for(int i=3; i<args.length;i++)
        {
            opnd+=args[i];
            opnd+=" ";
        }
        opnd = opnd.substring(0, Math.min(opnd.length(),opnd.length()-1)); //remove o espaco no fim do argumento

        try {
            InetAddress address = InetAddress.getByName(mcast_addr);
            echoSock = new Socket(address,mcast_port);
            PrintWriter outToServer = new PrintWriter(echoSock.getOutputStream(),true);
            BufferedReader inFromServer = new BufferedReader(new InputStreamReader(echoSock.getInputStream()));
            outToServer.println("");
            outToServer.flush();
            outToServer.println(oper+" "+opnd);
            outToServer.flush();
            System.out.println("sending: "+oper+" "+opnd);
            String result = inFromServer.readLine();
            System.out.println(oper+ " "+opnd +": "+ result);
            echoSock.close();
        } catch (IOException e) {
            System.out.println(e.getMessage());
            e.printStackTrace();
        }
        return;
    }
}
