package TCP.server;

import TCP.server.Database;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.InetAddress;
import java.net.ServerSocket;
import java.net.Socket;

public class Server {
    public static void main(String[] args) throws IOException{

        if(args.length != 1){
            System.out.println("Wrong Arguments!");
            System.out.println("Usage: java Server <srvc_port>");
            System.exit(-1);
        }

        /*
            Variable values for testing purposes
            @srvcPort - 4445
         */

        InetAddress srvcAddr = InetAddress.getLocalHost();
        int srvcPort = Integer.parseInt(args[0]);

        ServerSocket S = new ServerSocket(srvcPort);

        System.out.println("Creating database...");
        Database data = new Database();

        System.out.println("Booting server...");
        System.out.println("External IP: " + srvcAddr.getHostAddress());

        // Receber informacao
        Socket received = S.accept();

        BufferedReader receivedBuffer = new BufferedReader(new InputStreamReader(received.getInputStream()));
        PrintWriter output = new PrintWriter(received.getOutputStream(), true);

        while(true){

            String receivedS = new String(receivedBuffer.readLine());
            System.out.println("Received: "+receivedS);

            // Obter informacao
            InetAddress ip = received.getInetAddress();
            int port = received.getPort();

            // REGEX //
            // LOOKUP - (?i)^lookup [a-z0-9]{2}-[a-z0-9]{2}-[a-z0-9]{2}$
            // REGISTER - (?i)^register [a-z0-9]{2}-[a-z0-9]{2}-[a-z0-9]{2} [a-z]+$
            // EXIT - (?i)^exit$

            if( receivedS.matches("(?i)^lookup [a-z0-9]{2}-[a-z0-9]{2}-[a-z0-9]{2}$") ){                      // LOOKUP
                String plateNo = new String( receivedS.substring("LOOKUP ".length()));
                String answerS = new String( plateNo.trim() + " " + data.lookup(plateNo.trim()) );

                output.println(answerS);
                output.flush();

                System.out.println("Wrote " + answerS);
            } else if( receivedS.matches("(?i)^register [a-z0-9]{2}-[a-z0-9]{2}-[a-z0-9]{2} [a-z]+$") ){      // REGISTER
                String plateNo = new String( receivedS.substring( "REGISTER ".length(), "REGISTER ".length()+8));
                String owner = new String( receivedS.substring( "REGISTER ".length()+9) );
                int answerS = data.register( plateNo.trim(), owner.trim() );

                output.println(answerS);
                output.flush();

                System.out.println("Wrote " + answerS);
            } else if( receivedS.matches("(?i)^exit$") ){                                                     // END COMUNICATIONS
                output.println("ACK");
                output.flush();

                System.out.println("Wrote ACK");
                break;
            } else {                                                                                          // WHEN ALL ELSE FAILS
                output.println("PLEASE TALK TO ME");
                output.flush();

                System.out.println("Wrote PLEASE TALK TO ME");
            }
        }

        output.close();
        receivedBuffer.close();
        S.close();
        System.out.println("Shutting down server...");
    }
}
