package Common;

import java.io.IOException;
import java.net.DatagramPacket;
import java.net.InetAddress;
import java.net.MulticastSocket;

public class MsgReceiver {

    private static final int MAX_PACKET_SIZE = 65535;

    static public boolean connect(MulticastSocket socket, InetAddress address){
        try {
            socket.joinGroup(address);
        } catch (Exception e) {
            System.out.println("oh no :( \n\t "+e.toString());
            e.printStackTrace();
            return false;
        }
        return true;
    }

    static public boolean disconnect(MulticastSocket socket, InetAddress address){
        try {
            socket.leaveGroup(address);
        } catch (Exception e) {
            System.out.println("Failure to disconnect "+e.toString());
            e.printStackTrace();
            return false;
        }
        socket.close();
        return true;
    }

    static public String receive(MulticastSocket socket)
    {
        byte buf[] = new byte[MAX_PACKET_SIZE];
        DatagramPacket packet = new DatagramPacket(buf, buf.length);
        while(true)
        {
            try {
                socket.receive(packet);
            } catch (IOException e) {
                e.printStackTrace();
                continue;
            }
            String receivedS = new String(packet.getData(), 0, packet.getLength()).trim();

            return receivedS;
        }
    }
}
