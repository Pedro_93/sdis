package Common;

/**
 * Created by Pedro on 28-03-2014.
 */
public class Chunk {
    public char[] fileID;
    public int chunkNo;
    public int repDeg;
    public byte[] chunkBody;

    public Chunk(char[] fileID)
    {
        this.fileID=fileID;
        this.chunkNo=-1;
        this.repDeg=-1;
        this.chunkBody=null;
    }

    public Chunk(char[] fileID, int chunkNo)
    {
        this.fileID=fileID;
        this.chunkNo=chunkNo;
        this.repDeg=-1;
        this.chunkBody=null;
    }

    public Chunk(char[] fileID, int chunkNo, int repDeg, byte[] body)
    {
        this.fileID=fileID;
        this.chunkNo=chunkNo;
        this.repDeg=repDeg;
        this.chunkBody=body;
    }
}
