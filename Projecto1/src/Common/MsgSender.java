package Common;

import java.io.IOException;
import java.net.DatagramPacket;
import java.net.InetAddress;
import java.net.MulticastSocket;

public class MsgSender {

    static public boolean connect(MulticastSocket socket, InetAddress address){
        try {
            socket.joinGroup(address);
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
        return true;
    }

    static public boolean disconnect(MulticastSocket socket, InetAddress address){
        try {
            socket.leaveGroup(address);
        } catch (Exception e) {
            e.printStackTrace();
            socket.close();
            return false;
        }
        socket.close();
        return true;
    }

    static public void send(MulticastSocket socket, byte[] message, InetAddress address, int port){
        DatagramPacket packet = new DatagramPacket(message, message.length, address, port);
            try {
                socket.send(packet);
            } catch (IOException e) {
                System.out.println(e.toString());
                e.printStackTrace();
            }
    }

}
