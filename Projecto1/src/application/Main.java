package application;

import Server.Database;
import Server.ListenerMC;
import Server.ListenerMDB;
import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.stage.Stage;
import javafx.scene.Parent;
import javafx.scene.Scene;

public class Main extends Application {

    public static String MCchannel,MDRchannel,MDBchannel;
    public static int MCport, MDRport, MDBport;

    Database db = Database.getInstance();


    @Override
	public void start(Stage primaryStage) {
		try {
			Parent root = FXMLLoader.load(getClass().getResource("/application/GUI.fxml"));
			Scene scene = new Scene(root);
			scene.getStylesheets().add(getClass().getResource("application.css").toExternalForm());
			primaryStage.setScene(scene);
			primaryStage.show();
		} catch(Exception e) {
			e.printStackTrace();
		}
	}
	
	public static void main(String[] args) {

        ListenerMC listMC=null;
        ListenerMDB listMDB=null;
        MCchannel=args[0];
        MCport=Integer.parseInt(args[1]);
        MDBchannel=args[2];
        MDBport=Integer.parseInt(args[3]);
        MDRchannel=args[4];
        MDRport=Integer.parseInt(args[5]);

        listMC = new ListenerMC(MCchannel,MCport, MDRchannel, MDRport);
        listMDB = new ListenerMDB(MDBchannel,MDBport,MCchannel,MCport);
        if(listMC != null) listMC.start();
        if(listMDB != null) listMDB.start();
		launch(args);
	}
}
