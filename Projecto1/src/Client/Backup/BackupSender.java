package Client.Backup;

import java.io.File;
import java.io.FileInputStream;
import java.net.InetAddress;
import java.nio.ByteBuffer;
import java.security.MessageDigest;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

public class BackupSender extends Thread {

    private static final long MAX_FILE_SIZE = 64000000000L;
    private static final int MAX_CHUNK_SIZE = 64000;

    private int replicationDegree;
    private File fileToBackup;
    private int chunkAmmount;
    private List<byte[]> chunks;
    private int lastChunkSize;

    private char[] fileId;
    private static char[] version= {'1','.','0'};

    private InetAddress MDBaddress;
    private InetAddress MCaddress;
    private int MDBport;
    private int MCport;

    public BackupSender(String filePath, int replicationDegree, String MDBchannel, int MDBport, String MCchannel, int MCport) throws Exception{
        super("BackupSender");

        // Configuring file definitions //
        if (filePath == null)
            throw new Exception("No such file in the requested path!");

        this.replicationDegree = replicationDegree;
        if(this.replicationDegree <= 0)
            throw new Exception("Replication degree must be bigger than 0!");

        fileToBackup = new File(filePath);

        if(!fileToBackup.exists())
            throw new Exception("No such file!");
        if (fileToBackup.length() > MAX_FILE_SIZE)
            throw new Exception("File size exceeded!");

        if(fileToBackup.length() < MAX_CHUNK_SIZE)
            this.chunkAmmount = 1;
        else {
            this.chunkAmmount = (int)Math.ceil((double)fileToBackup.length()/MAX_CHUNK_SIZE);

            if(fileToBackup.length() % MAX_CHUNK_SIZE == 0)
                this.chunkAmmount++;
        }

        // Creating the chunk array //
        chunks = new ArrayList<byte[]>();

        for (int i = 0; i < chunkAmmount-1; i++) {
            chunks.add(new byte[64000]);
        }
        lastChunkSize = (int)fileToBackup.length() - 64000*(chunkAmmount-1);
        chunks.add(new byte[lastChunkSize]);

        // fileId generation //
        try{
            byte[] lastMod = ByteBuffer.allocate(8).putLong(fileToBackup.lastModified()).array();
            MessageDigest digest = MessageDigest.getInstance("SHA-256");
            digest.update(lastMod, 0, 8);
            byte[] hash = digest.digest();
            fileId = new char[64];
            for (int i = 0; i < hash.length; i++) {
                fileId[i*2] = hashToChar((hash[i] & 0xF0) >> 4);
                fileId[i*2 + 1] = hashToChar(hash[i] & 0x0F);
            }
            // System.out.println("Look at this hash: " + fileId.toString());
        } catch(Exception e){
            e.printStackTrace();
        }

        // Socket configurations //
        try {
            this.MDBport=MDBport;
            this.MCport=MCport;
            this.MDBaddress=InetAddress.getByName(MDBchannel);
            this.MCaddress=InetAddress.getByName(MCchannel);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    // Aux function to create fileId //
    private char hashToChar( int i ){
        if(i>=0 && i<10)
            return (char)('0'+i);
        else
            return (char)('A'+i-10);
    }

    // "main"-like method //
    @Override
    public void run() {
        super.run();
        FileInputStream input = null;

        // System.out.println("Trying to open file on: " + this.filePath);
        try {
            input = new FileInputStream(fileToBackup);

            for (int i = 0; i < chunkAmmount; i++) {
                try {
                    if (i != chunkAmmount - 1)
                        input.read(chunks.get(i), 0, MAX_CHUNK_SIZE);
                    else
                        input.read(chunks.get(i), 0, lastChunkSize);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
            this.interrupt();
        } finally {
            try {
                if (input != null) input.close();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        // Create a thread pool using Java Executor Services
        ExecutorService es = Executors.newCachedThreadPool();

        // System.out.println("Requesting " + chunkAmmount + " threads...");

        for (int i = 0; i < chunkAmmount; i++) {
            // System.out.println("Sending thread no. " + (i + 1));
            es.execute(new ChunkSender(fileId, replicationDegree, i, version, chunks.get(i), MDBaddress, MDBport, MCaddress, MCport));
        }
        es.shutdown();

        try {
            es.awaitTermination(20, TimeUnit.SECONDS);
        } catch (Exception e) {
            e.printStackTrace();
        }

        // Here all threads have completed either by timeout or because they did their job :)
    }
}
