package Client.Backup;

import Common.MsgReceiver;
import Common.MsgSender;
import Server.Database;

import java.net.MulticastSocket;
import java.net.InetAddress;

public class ChunkSender extends Thread{

    private static final int MAX_TRIES = 5;

    private char[] fileId;
    private byte[] messageBody;
    private char[] version;
    private int chunkNo;
    private int replicationDegree;

    private InetAddress MDBaddress;
    private MulticastSocket MDBsocket;
    private InetAddress MCaddress;
    private MulticastSocket MCsocket;
    private int MDBport,MCport;

    private String receivedMessage;
    private String receivedMsgType;
    private char[] responseVersion;
    private char[] responseFileId;
    private int responseChunkNo;


    public ChunkSender(char[] fileId, int replicationDegree, int chunkNo, char[] version, byte[] body, InetAddress MDBaddress, int MDBport, InetAddress MCaddress, int MCport) {
        super("ChunkSender");

        // Configura as sockets a usar //
        this.MDBaddress = MDBaddress;
        this.MDBport = MDBport;
        this.MCaddress = MCaddress;
        this.MCport = MCport;

        // Constantes de ficheiros //
        this.fileId=fileId;
        this.replicationDegree=replicationDegree;
        this.chunkNo=chunkNo;
        this.messageBody=body;
        this.version=version;

        // Sockets //
        try {
            this.MDBsocket = new MulticastSocket(this.MDBport);
        } catch (Exception e) {
            e.printStackTrace();
        }

        try {
            this.MCsocket = new MulticastSocket(this.MCport);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    // "main"-like method
    @Override
    public void run() {
        super.run();
        // Connect to the db
        Database db = Database.getInstance();

        // Prepare the message to send //
        byte[] message  = buildMessage();
        MsgSender.connect(MDBsocket,MDBaddress);
        MsgReceiver.connect(MCsocket,MCaddress);

        // Register the message in the database
        db.registerMsg(message);
        System.out.println("Message: \n"+message);

        // Prepare to receive an answer //
        boolean response = false;
        int noTries = 0;
        long time2w8 = 500;

        while(!response) {
            int noResponses = 0;
            long currTime = System.currentTimeMillis();

            // Send the message
            MsgSender.send(MDBsocket, message, MDBaddress, MDBport);

            while(noResponses != replicationDegree){
                receivedMessage = MsgReceiver.receive(MCsocket);
                receivedMsgType = receivedMessage.split(" ")[0];

                if(receivedMsgType.equals("STORED"))
                {
                    responseVersion = new String(receivedMessage.split(" ")[1]).toCharArray();
                    responseFileId  = new String(receivedMessage.split(" ")[2]).toCharArray();
                    responseChunkNo = Integer.parseInt(receivedMessage.split(" ")[3]);
                    if(responseVersion.equals(version) && responseFileId.equals(fileId) && responseChunkNo==chunkNo)
                        noResponses++;
                }

                if( System.currentTimeMillis() - currTime > time2w8 )
                    break;
            }

            if( noResponses >= replicationDegree )
                response = true;
            else {
                time2w8 *= 2;
                noTries++;
            }

            if( noTries == MAX_TRIES )
                response = true;
        }

        MsgSender.disconnect(MDBsocket, MDBaddress);
        MsgReceiver.disconnect(MCsocket,MCaddress);
    }

    private byte[] buildMessage() {
        String Version =new String(version);
        String fileID = new String(fileId);
        String ChunkNo = Integer.toString(chunkNo);
        String RepDeg = Integer.toString(replicationDegree);
        String header = "PUTCHUNK "+Version +" "+ fileID + " " + ChunkNo+" "+RepDeg + "\r\n\r\n";
        byte[] headerB = header.getBytes();
        byte[] result = new byte[messageBody.length+headerB.length];
        System.arraycopy(headerB, 0, result, 0, headerB.length);
        System.arraycopy(messageBody, 0, result, headerB.length, messageBody.length);
        return result;
    }
}