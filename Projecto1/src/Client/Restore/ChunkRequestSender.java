package Client.Restore;

import Common.MsgReceiver;
import Common.MsgSender;
import Server.Database;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.net.InetAddress;
import java.net.MulticastSocket;

public class ChunkRequestSender extends Thread{

    private char[] fileId;
    private char[] version;
    private int chunkNo;

    private InetAddress MDRaddress;
    private MulticastSocket MDRsocket;
    private InetAddress MCaddress;
    private MulticastSocket MCsocket;
    private int MDRport,MCport;

    // GETCHUNK <Version> <FileId> <ChunkNo> <CRLF> <CRLF>

    public ChunkRequestSender(int chunkNo, char[] fileId, char[] version, InetAddress MCaddress, int MCport, InetAddress MDRaddress, int MDRport) {
        super("ChunkRequestSender");
        this.fileId = fileId;
        this.chunkNo = chunkNo;
        this.version = version;

        this.MDRaddress = MDRaddress;
        try {
            this.MDRsocket = new MulticastSocket(this.MDRport);
        } catch (IOException e) {
            e.printStackTrace();
        }
        this.MDRport = MDRport;

        this.MCaddress = MCaddress;
        try {
            this.MCsocket = new MulticastSocket(this.MCport);
        } catch (IOException e) {
            e.printStackTrace();
        }
        ;
        this.MCport = MCport;
    }

    @Override
    public void run(){
        super.run();
        byte[] message  = buildMessage();
        MsgSender.connect(MCsocket,MCaddress);
        MsgReceiver.connect(MDRsocket,MDRaddress);
        MsgSender.send(MCsocket, message, MCaddress, MCport);

        // Prepare to receive an answer //
        boolean response = false;
        String receivedMessage = null;


        while(!response) {
            receivedMessage = MsgReceiver.receive(MCsocket);
            String receivedMsgType = receivedMessage.split(" ")[0];

            char[] responseVersion;
            char[] responseFileId;
            int responseChunkNo;

            if(receivedMsgType.equals("CHUNK")) {
                responseVersion = new String(receivedMessage.split(" ")[1]).toCharArray();
                responseFileId  = new String(receivedMessage.split(" ")[2]).toCharArray();
                responseChunkNo = Integer.parseInt(receivedMessage.split(" ")[3]);
                if(responseVersion.equals(version) && responseFileId.equals(fileId) && responseChunkNo==chunkNo)
                    response = true;
            }
        }

        byte[] body;

        if( receivedMessage != null && receivedMessage.split("\\r\\n\\r\\n").length > 1 )
            body = receivedMessage.split("\\r\\n\\r\\n")[1].getBytes();
        else
            body = null;

        // Get current db instance //
        Database db = Database.getInstance();

        // Save register in the database //
        db.registerChunk(new String(fileId), chunkNo);

        // Save file to disk
        File f = new File("CHUNKS\\"+fileId+"_CHUNK"+chunkNo);
        f.getParentFile().mkdirs();

        FileOutputStream fOut = null;

        try {
            f.createNewFile();
            fOut = new FileOutputStream(f);
            fOut.write(body, 0, body.length);
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if( fOut != null ) try {
                fOut.close();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    private byte[] buildMessage() {
        String Version =new String(version);
        String fileID = new String(fileId);
        String ChunkNo = Integer.toString(chunkNo);
        String header = "GETCHUNK "+Version +" "+ fileID + " " + ChunkNo+"\r\n\r\n";
        byte[] headerB = header.getBytes();
        byte[] result = new byte[headerB.length];
        System.arraycopy(headerB, 0, result, 0, headerB.length);
        return result;
    }


}
