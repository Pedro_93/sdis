package Client.Restore;

import Common.MsgReceiver;
import Common.MsgSender;
import Server.Database;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.net.InetAddress;
import java.net.MulticastSocket;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

/*
    Sends to MC: GETCHUNK <Version> <FileId> <ChunkNo> <CRLF> <CRLF>
    receives from MDR: CHUNK <Version> <FileId> <ChunkNo> <CRLF> <CRLF> <Body>
 */
public class RestoreSender extends Thread {

    private char[] fileId;
    private int chunkAmmt;
    private String filePath;

    private InetAddress MDRaddress;
    private MulticastSocket MDRsocket;
    private InetAddress MCaddress;
    private MulticastSocket MCsocket;
    private int MDRport,MCport;

    public RestoreSender(String filePath, char[] fileId, String MCchannel, int MCport, String MDRchannel, int MDRport ){
        super("ChunkResquestSender");
        this.filePath = filePath;
        this.fileId = fileId;
        this.chunkAmmt = -1;

        try {
            this.MDRport=MDRport;
            this.MCport=MCport;
            this.MDRaddress=InetAddress.getByName(MDRchannel);
            this.MCaddress=InetAddress.getByName(MCchannel);
            MDRsocket=new MulticastSocket(MDRport);
            MCsocket=new MulticastSocket(MCport);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public RestoreSender(String filePath, char[] fileId, int chunkAmmt, String MCchannel, int MCport, String MDRchannel, int MDRport){
        this(filePath, fileId,MCchannel,MCport,MDRchannel,MDRport);
        this.chunkAmmt = chunkAmmt;
    }

    @Override
    public void run(){
        super.run();

        if(chunkAmmt == -1)
        {
            int chunkTest=0;
            ExecutorService chunkReqEs = Executors.newCachedThreadPool();
            while(true)
            {
                chunkReqEs.execute(new ChunkRequestSender(chunkTest,fileId,"1.0".toCharArray(),MCaddress,MCport,MDRaddress,MDRport));
                chunkReqEs.shutdown();

                try {
                    chunkReqEs.awaitTermination(5, TimeUnit.SECONDS);
                } catch(Exception e){
                    e.printStackTrace();
                }
                if(Database.getInstance().lookupChunk(fileId.toString(),chunkTest))
                {
                    //Found chunk reference in BD
                    File file = new File("CHUNKS\\"+fileId+"_CHUNK"+chunkTest);
                    if(file.length() < 64000)
                    {
                        break;
                    }
                    else{
                        chunkTest++;
                    }
                }
                else
                {
                    //Chunk not found
                    return;
                }
            }
        }
        else
        {
        // Call chunkAmmt threads so you can receive the chunks
        // Need to wait on all threads to finish //
        ExecutorService es = Executors.newCachedThreadPool();

        MsgSender.connect(MCsocket, MCaddress);
        MsgReceiver.connect(MDRsocket, MDRaddress);

        for(int i=0; i<chunkAmmt; i++){
            // System.out.println("Setting up thread no. "+(i+1));
            es.execute(new ChunkRequestSender(i, fileId, "1.0".toCharArray(),MCaddress,MCport,MDRaddress,MDRport));
        }
        es.shutdown();

        try {
            es.awaitTermination(20, TimeUnit.SECONDS);
        } catch(Exception e){
            e.printStackTrace();
        }
        }
        MsgSender.disconnect(MCsocket, MCaddress);
        MsgReceiver.disconnect(MDRsocket, MDRaddress);

        // Here all threads have completed either by timeout or because they did their job :)

        // After all threads got their files saved to the disk, rebuild the file
        File fOut = new File(filePath);
        FileOutputStream output = null;
        try {
            output = new FileOutputStream(fOut);
        } catch (Exception e) {
            e.printStackTrace();
        }

        for(int i=0; i<chunkAmmt; i++){
            try {
                FileInputStream input = new FileInputStream("CHUNKS\\Chunk"+i);

                byte[] outBuf = new byte[input.available()];
                input.read(outBuf, 0, input.available());

                output.write(outBuf);
                input.close();
            } catch(Exception e){
                e.printStackTrace();
            }
        }

        try {
            if (output != null) output.close();
        } catch (Exception e) {
            e.printStackTrace();
        }

    }
}
