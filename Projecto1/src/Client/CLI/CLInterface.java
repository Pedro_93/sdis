package Client.CLI;

import Client.Backup.BackupSender;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class CLInterface {

    public static void main(String[] args){

        if(args.length != 6){
            System.out.println("Wrong Arguments!");
            System.out.println("Usage: java CLInterface <MC_address> <MC_port> <MDB_address> <MDB_port> <MDR_address> <MDR_port>");
            System.exit(-1);
        }

        String input = "EXIT";

        do{
            BufferedReader inputStream = new BufferedReader(new InputStreamReader(System.in));
            try {
                input = inputStream.readLine();
                parseInput(input.trim());
            }catch(Exception e){
                System.out.println("Error while reading from console!");
                e.printStackTrace();
            }

        }while( !input.equals("EXIT"));
    }

    private static void parseInput(String input){
        // For future Reference
        // Pattern putChunkPat = Pattern.compile("PUTCHUNK [0-9][.][0-9] [a-f0-9]+ [0-9] [0-9].*", Pattern.CASE_INSENSITIVE & Pattern.DOTALL);
        Pattern putChunkPat = Pattern.compile("^PUTCHUNK [a-zA-Z0-9_.-\\\\]+ [0-9].*$", Pattern.CASE_INSENSITIVE & Pattern.DOTALL);
        Matcher putChunkMatch = putChunkPat.matcher(input);


        if( putChunkMatch.matches() ){
            System.out.println("Creating BackupSender thread!");
            try {
                //new BackupSender(input.split(" ", -1)[1], Integer.parseInt(input.split(" ", -1)[2])).run();
            }catch(Exception e){
                System.out.println("Error creating BackupSender!");
                e.printStackTrace();
            }
        } else
            System.out.println(input);
    }
}
