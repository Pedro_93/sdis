package Client;

import Client.Backup.BackupSender;

import java.net.InetAddress;
import java.net.MulticastSocket;

public class Sender extends Thread {

    private String msgType;
    private String filePath;
    private int replicationDegree;
    private char[] fileId;
    private int chunkAmmt;

    private int MDBPort;
    private int MCPort;

    private InetAddress MDBAddress;
    private InetAddress MCAddress;

    private MulticastSocket MDBsocket;
    private MulticastSocket MCsocket;

    private String MDBchannel;
    private String MCchannel;

    public Sender(String msgType, String filePath, int replicationDegree, String MDBchannel, int MDBPort, String MCchannel, int MCPort){
        super("Sender");
        this.msgType = msgType;
        this.filePath = filePath;
        this.replicationDegree = replicationDegree;
        this.MCPort = MCPort;
        this.MDBPort = MDBPort;
        this.MDBchannel = MDBchannel;
        this.MCchannel = MCchannel;

        try {
            this.MDBAddress = InetAddress.getByName(MDBchannel);
            this.MCAddress = InetAddress.getByName(MCchannel);
            MDBsocket = new MulticastSocket(MDBPort);
            MCsocket = new MulticastSocket(MCPort);
        } catch(Exception e){
            e.printStackTrace();
        }
    }

    public Sender(String msgType, String filePath, int replicationDegree, char[] fileId, String MDBchannel, int MDBPort, String MCchannel, int MCPort){
        this(msgType, filePath, replicationDegree, MDBchannel, MDBPort, MCchannel, MCPort);
        this.fileId = fileId;
        this.chunkAmmt = -1;
    }

    public Sender(String msgType, String filePath, int replicationDegree, char[] fileId, int chunkAmmt, String MDBchannel, int MDBPort, String MCchannel, int MCPort){
        this(msgType, filePath, replicationDegree, fileId, MDBchannel, MDBPort, MCchannel, MCPort);
        this.chunkAmmt = chunkAmmt;
    }

    @Override
    public void run(){
        super.run();

        if( msgType.equals("BACKUP"))
            try {
                new BackupSender(filePath, replicationDegree, MDBchannel, MDBPort, MCchannel, MCPort).run();
            } catch(Exception e){
                e.printStackTrace();
            }
        else if( msgType.equals("RESTORE"))
            try{
                if( chunkAmmt == -1)
                {  //new RestoreSender(filePath, fileId).run();
                 }
                else
                { //new RestoreSender(filePath, fileId, chunkAmmt).run();
                }
            } catch(Exception e){
                e.printStackTrace();
            }
        else if( msgType.equals("DELETE"))
            System.out.println("DELETE REQUEST RECEIVED!");
        else if( msgType.equals("RECLAIM"))
            System.out.println("RECLAIM REQUEST RECEIVED!");
    }
}
