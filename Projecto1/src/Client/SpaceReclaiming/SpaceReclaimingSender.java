package Client.SpaceReclaiming;

import Common.MsgSender;

import java.io.IOException;
import java.net.InetAddress;
import java.net.MulticastSocket;
import java.net.UnknownHostException;

/*
REMOVED <Version> <FileId> <ChunkNo> <CRLF> <CRLF>
*/
public class SpaceReclaimingSender extends Thread{

    private char[] FileId;
    private char[] Version;
    private int ChunkNo;
    private static char[] CRLF = {0xD,0xA};
    private InetAddress MCaddress;
    private MulticastSocket MCsocket;
    private int MCport;

    public SpaceReclaimingSender(char[] FileId, char[] Version, int ChunkNo, String MCchannel, int MCport){
        super("SpaceReclaimingSender");
        this.FileId=FileId;
        this.Version=Version;
        this.ChunkNo=ChunkNo;
        this.MCport=MCport;

        try {
            this.MCaddress= InetAddress.getByName("239.0.0.1");
            this.MCsocket=new MulticastSocket();
        } catch (UnknownHostException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void run()
    {
        super.run();
        String message  = buildMessage();
        MsgSender.connect(MCsocket, MCaddress);
        //MsgSender.send(MCsocket, message, MCaddress, MCport);
        MsgSender.disconnect(MCsocket, MCaddress);
    }

    private String buildMessage() {
        try {
            return "REMOVED " +new String(Version)+" " +new String(FileId)+" "+ChunkNo+new String(CRLF) + new String(CRLF);
        } catch(Exception e){
            e.printStackTrace();
        }
        return null;
    }
}
