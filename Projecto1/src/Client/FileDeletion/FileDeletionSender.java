package Client.FileDeletion;

import Common.MsgSender;

import java.net.InetAddress;
import java.net.MulticastSocket;

/*
DELETE <FileId> <CRLF> <CRLF>
*/

public class FileDeletionSender extends Thread{

    public static final int MAX_RESENDS = 10;

    private char[] fileId;
    private static char[] CRLF = {0xD,0xA};

    private InetAddress MCaddress;
    private MulticastSocket MCsocket;
    private int MCport;

    public FileDeletionSender(char[] fileId, String MCchannel, int MCport){
        super("FileDeletionSender");
        this.MCport=MCport;
        this.fileId=fileId;

        // Create MCSocket
        try {
            this.MCaddress=InetAddress.getByName(MCchannel);
            this.MCsocket=new MulticastSocket(this.MCport);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void run(){
        super.run();
        byte[] message = buildMessage();
        MsgSender.connect(MCsocket, MCaddress);

        for(int i=0;i<MAX_RESENDS;i++) {
            MsgSender.send(MCsocket, message, MCaddress, MCport);
            try {
                this.sleep(500);
            } catch(InterruptedException ex) {
                Thread.currentThread().interrupt();
            }
        }
        MsgSender.disconnect(MCsocket, MCaddress);
    }

    private byte[] buildMessage() {
        String header = "DELETE "+fileId+"\r\n\r\n";
        byte[] headerB = header.getBytes();
        byte[] result = new byte[headerB.length];
        System.arraycopy(headerB, 0, result, 0, headerB.length);
        return result;
    }
}