package Server;

import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.HashMap;

// Implements singleton pattern
public class Database {
    private static volatile Database instance = null;

    private HashMap<String, ArrayList<Integer>> db;
    private ArrayList<byte[]> localMsgs;

    private Database(){
        this.db = new HashMap<String, ArrayList<Integer>>();
        localMsgs = new ArrayList<byte[]>();
    }

    // Get the current instance of the database
    public static Database getInstance(){
        if(instance == null){
            synchronized (Database.class){
                if(instance == null){
                    instance = new Database();
                }
            }
        }
        return instance;
    }

    // Test reasons only //
    public ArrayList<byte[]> getMsgs(){
        return this.localMsgs;
    }

    // Registers a message into the database
    // On success returns the new db size (always >0)
    // On fail (if the message already exists) returns -1
    public int registerMsg( byte[] msg){
        boolean hasMsg = localMsgs.contains(msg);

        if(hasMsg)
            return -1;
        else
            localMsgs.add(msg);


        return this.localMsgs.size();
    }

    // Checks if a msg is in the database
    // true -> it is || false -> it isn't
    public boolean hasMsg( String msg ){
        for (int i = 0; i < localMsgs.size(); i++) {
            String test = new String(localMsgs.get(i), StandardCharsets.US_ASCII);
            if( test.equals(msg))
                return true;
        }
        return false;
    }

    // Removes a message from the database, if it exists
    public void removeMsg( byte[] msg ){
        if( hasMsg( new String(msg, StandardCharsets.US_ASCII)) )
            localMsgs.remove(msg);
    }

    // Returns:
    //  -1 if chunkNo is not within acceptable bounds
    //  -2 if fileId already in database and chunkNo already registered to that fileId
    //  new db.size() if successful (always >0)
    public int registerChunk( String fileId, Integer chunkNo ){
        if( chunkNo < 0 || chunkNo > 9 )
            return -1;

        ArrayList<Integer> chunks = this.db.get(fileId);

        // Se ja houver pelo menos 1 chunk para este fileId ve se o chunkNo e novo ou ja se encontra na bd
        if( chunks != null ) {
            if( !chunks.contains(chunkNo) )
                chunks.add(chunkNo);
            else
                return -2;
        } else {            // Se ainda nao existir cria o registo
            this.db.put(fileId, new ArrayList<Integer>());
            this.db.get(fileId).add(chunkNo);
        }

        return this.db.size();
    }

    // If fileId is not in the database returns -1, otherwise returns new database size (always >= 0)
    public int removeChunk( String fileId ){
        if( this.db.get(fileId) != null )
            this.db.remove( fileId );
        else
            return -1;

        return this.db.size();
    }

    // If fileId does not have chunkNo in its saved chunks returns -1, otherwise returns new database size (always >= 0)
    public int removeChunk( String[] fileId, Integer chunkNo ){
        ArrayList<Integer> chunks = this.db.get(fileId);

        if( chunks != null )
            if( chunks.contains(chunkNo) )
                chunks.remove(chunkNo);
            else
                return -1;
        else
            return -1;

        return this.db.size();
    }

    // Returns the current file database in a string array
    public ArrayList<String> lookupFiles(){
        ArrayList<String> result = new ArrayList<String>();
        for( String key : this.db.keySet() ) {
            result.add(key);
        }
        return result;
    }

    // Returns in an ArrayList all the chunkNo's related to a given fileId
    // In the case that fileId doesn't exist in the database it returns null
    public ArrayList<Integer> lookupChunks( String fileId ){
        ArrayList<Integer> chunks = this.db.get(fileId);
        if( chunks == null )
            return null;
        else
            return chunks;
    }

    // Returns true if the given chunkNo is in the database and related to the given fileId, false otherwise
    // In the case that fileId doesn't exist in the database it returns false
    public Boolean lookupChunk( String fileId, Integer chunkNo ){
        ArrayList<Integer> chunks = this.db.get(fileId);
        if( chunks != null )
            if( chunks.contains(chunkNo) )
                return true;
            else
                return false;
        else
            return false;
    }
}
