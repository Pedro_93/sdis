package Server.FileDeletion;

import Server.Database;

import java.io.File;
import java.util.ArrayList;

/**
 * Created by Pedro on 27-03-2014.
 */
public class FileDeletionReceiver extends Thread{

    private String message;
    private char[] fileId;

    public FileDeletionReceiver(String received)
    {
        this.message=received;
        fileId = new String(received.split("[ |\\r]")[1]).toCharArray();
        System.out.println(fileId);
    }

    public void run()
    {
        super.run();
        Database db = Database.getInstance();

        ArrayList<Integer> chunks = db.lookupChunks(fileId.toString());
        System.out.println("delete: "+chunks.size());
        if( chunks != null ){
            for(int i=0; i<chunks.size(); i++){
                File f = new File("CHUNKS\\"+fileId+"_CHUNK"+i);
                f.delete();
            }
        }
    }
}
