package Server;

import Common.MsgReceiver;
import Common.MsgSender;
import Server.Backup.BackupReceiver;

import java.net.InetAddress;
import java.net.MulticastSocket;

public class ListenerMDB extends Thread{

    private static final int MAX_PACKET_SIZE = 65536;

    InetAddress MDBAddress;
    InetAddress MCAddress;
    MulticastSocket MDBsocket;
    MulticastSocket MCsocket;
    private int MDBPort,MCPort;

    public ListenerMDB(String MDBchannel, int MDBPort, String MCchannel, int MCPort){
        super("ListenerMC");

        try {
            this.MDBPort=MDBPort;
            this.MCPort=MCPort;
            this.MDBAddress = InetAddress.getByName(MDBchannel);
            this.MCAddress = InetAddress.getByName(MCchannel);
            MDBsocket = new MulticastSocket(MDBPort);
            MCsocket = new MulticastSocket(MCPort);
        } catch(Exception e){
            e.printStackTrace();
        }
    }

    @Override
    public void run(){
        super.run();

        MsgReceiver.connect(MDBsocket,MDBAddress);
        MsgSender.connect(MCsocket,MCAddress);

        Database db = Database.getInstance();

        while(true){
            // Receber informacao
            String receivedS = new String(MsgReceiver.receive(MDBsocket));

            //System.out.println("Received: " + receivedS);

            // Get the message type
            String msgType = receivedS.split(" ")[0];


            if( msgType.equals("PUTCHUNK")) {
                if( !db.hasMsg(receivedS)) {
                    //System.out.println("PUTCHUNK Received!");
                    BackupReceiver backRec = new BackupReceiver(receivedS, MCAddress, MCPort);
                    backRec.run();
                } else
                   db.removeMsg( receivedS.getBytes() );
            }

            if( msgType.equals("EXIT"))
                break;
        }

        MsgReceiver.disconnect(MDBsocket, MDBAddress);
        MsgSender.disconnect(MCsocket, MCAddress);

        MDBsocket.close();
        MCsocket.close();
    }
}
