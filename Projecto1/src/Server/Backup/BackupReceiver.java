package Server.Backup;

import Common.MsgSender;
import Server.Database;


import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.InetAddress;
import java.net.MulticastSocket;
import java.nio.charset.Charset;

/**
 * Created by Pedro on 27-03-2014.
 * receives: PUTCHUNK <Version> <FileId> <ChunkNo> <ReplicationDeg> <CRLF> <CRLF> <Body>
 * sends: STORED <Version> <FileId> <ChunkNo> <CRLF> <CRLF>
 */
public class BackupReceiver extends Thread{

    private String message;
    private char[] version;
    private String fileId;
    private int chunkNo;
    private int replicationDegree;
    private static char[] CRLF = {0xD,0xA};
    private byte[] body;
    InetAddress MCaddress;
    MulticastSocket MCsocket;
    private int MCport;

    public BackupReceiver(String received, InetAddress MCAddress,int MCport) {
        this.message=received;

        this.MCaddress=MCAddress;
        try {
            this.MCsocket=new MulticastSocket(MCport);
        } catch (IOException e) {
            e.printStackTrace();
        }
        this.MCport=MCport;

        version = new String(received.split(" ")[1]).toCharArray();
        fileId  = new String(received.split(" ")[2]);
        chunkNo = Integer.parseInt(received.split(" ")[3]);
        replicationDegree = Integer.parseInt(received.split("[ |\\r]")[4]);

        String bodyS = received.split("\\r\\n\\r\\n")[1];

        body = new byte[bodyS.length()];
        System.arraycopy(bodyS.getBytes(), 0, body, 0, body.length);
    }

    @Override
    public void run(){
        super.run();
        //System.out.println("IN backupReceiver run!");
        byte[] response = buildResponseMessage();

        // Get current db instance //
        Database db = Database.getInstance();
        MsgSender.connect(MCsocket,MCaddress);

        // Store information about this file in the database
        // If the file isn't in the database yet, then ...
        if( !db.lookupChunk(fileId, chunkNo) ){
            // Save register in the database //
            db.registerChunk(fileId, chunkNo);

            // Save file to disk
            File f = new File("CHUNKS\\"+fileId+"_CHUNK"+chunkNo);
            f.getParentFile().mkdirs();

            FileOutputStream fOut = null;

            try {
                f.createNewFile();
                fOut = new FileOutputStream(f);
                fOut.write(body, 0, body.length);
            } catch (Exception e) {
                e.printStackTrace();
            } finally {
                if( fOut != null ) try {
                    fOut.close();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            // Answer with stored
            MsgSender.send(MCsocket, response, MCaddress, MCport);
        }

        MsgSender.disconnect(MCsocket,MCaddress);
        // Else: If it was already in the database the there is nothing more to do
    }

    private byte[] buildResponseMessage() {
        String Version =new String(version);
        String fileID = new String(fileId);
        String ChunkNo = Integer.toString(chunkNo);
        String header = "STORED "+Version +" "+ fileID + " " + ChunkNo + "\r\n\r\n";
        byte[] result = header.getBytes();
        return result;
    }
}
