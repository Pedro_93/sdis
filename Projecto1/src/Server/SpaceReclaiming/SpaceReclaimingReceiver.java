package Server.SpaceReclaiming;

/**
 * Created by Pedro on 27-03-2014.
 */
public class SpaceReclaimingReceiver extends Thread{

    private String message;
    private char[] version;
    private char[] fileId;
    private int chunkNo;
    private static char[] CRLF = {0xD,0xA};

    public SpaceReclaimingReceiver(String received)
    {
        this.message=received;
        version = new String(received.split(" ")[1]).toCharArray();
        fileId  = new String(received.split(" ")[2]).toCharArray();
        chunkNo = Integer.parseInt(received.split("[ |\\r]")[3]);
    }

    public void run()
    {
        super.run();
    }
}
