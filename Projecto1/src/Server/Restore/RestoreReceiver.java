package Server.Restore;

import Common.MsgSender;
import Server.Database;

import java.io.*;
import java.net.InetAddress;
import java.net.MulticastSocket;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

/**
 * Created by Pedro on 27-03-2014.
 * CHUNK <Version> <FileId> <ChunkNo> <CRLF> <CRLF> <Body>

 */
public class RestoreReceiver extends Thread{

    private String message;
    private char[] version;
    private char[] fileId;
    private int chunkNo;
    private byte[] messageBody;
    private static char[] CRLF = {0xD,0xA};
    InetAddress MDRaddress;
    MulticastSocket MDRsocket;
    private int MDRport;

    public RestoreReceiver(String received, InetAddress MDRAddress,int MDRport)
    {
        this.message=received;
        this.MDRaddress=MDRAddress;
        this.MDRport=MDRport;
        try {
            this.MDRsocket=new MulticastSocket(MDRport);
        } catch (IOException e) {
            e.printStackTrace();
        }
        try {
            version = new String(received.split(" ")[1].getBytes(),"UTF-8").toCharArray();
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
        fileId  = new String(received.split(" ")[2]).toCharArray();
        chunkNo = Integer.parseInt(received.split("[ |\\r]")[3]);
    }

    public void run()
    {
        Database db = Database.getInstance();
        if(db.lookupChunk(fileId.toString(),chunkNo))
        {
            File file = new File("CHUNKS\\"+fileId+"_CHUNK"+chunkNo);
            ByteArrayOutputStream ous = null;
            InputStream ios = null;
            try {
                byte[] buffer = new byte[4096];
                ous = new ByteArrayOutputStream();
                ios = new FileInputStream(file);
                int read = 0;
                int pointer =0;
                while ( (read = ios.read(buffer)) != -1 ) {
                    ous.write(messageBody, pointer, read);
                    pointer+=read;
                }
            } catch (IOException e) {
                e.printStackTrace();
            } finally {
                //Close all streams
                try {
                    if ( ous != null )
                        ous.close();
                } catch ( IOException e) {
                }
                try {
                    if ( ios != null )
                        ios.close();
                } catch ( IOException e) {
                }
            }

            byte[] response = buildResponseMessage();
            MsgSender.connect(MDRsocket,MDRaddress);
            MsgSender.send(MDRsocket,response,MDRaddress,MDRport);
            MsgSender.disconnect(MDRsocket,MDRaddress);
        }
    }

    private byte[] buildResponseMessage() {
        String Version =new String(version);
        String fileID = new String(fileId);
        String ChunkNo = Integer.toString(chunkNo);
        String header = "CHUNK "+Version +" "+ fileID + " " + ChunkNo + "\\r\\n\\r\\n";
        byte[] headerB = header.getBytes();
        byte[] result = new byte[messageBody.length+headerB.length+1];
        System.arraycopy(headerB, 0, result, 0, headerB.length);
        System.arraycopy(messageBody, 0, result, headerB.length+1, messageBody.length);
        return result;
    }
}
