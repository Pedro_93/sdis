package Server;

import Common.MsgReceiver;
import Common.MsgSender;
import Server.FileDeletion.FileDeletionReceiver;
import Server.Restore.RestoreReceiver;
import Server.SpaceReclaiming.SpaceReclaimingReceiver;

import java.net.InetAddress;
import java.net.MulticastSocket;

public class ListenerMC extends Thread{

    private static final int MAX_PACKET_SIZE = 65536;

    InetAddress MCAddress;
    InetAddress MDRAddress;
    MulticastSocket MCsocket;
    private int MCPort;
    private int MDRPort;

    public ListenerMC(String MCchannel, int MCPort, String MDRAddress, int MDRPort){
        super("ListenerMC");

        try {
            this.MCPort = MCPort;
            this.MDRPort = MDRPort;
            this.MCAddress = InetAddress.getByName(MCchannel);
            this.MDRAddress = InetAddress.getByName(MDRAddress);
            MCsocket = new MulticastSocket(this.MCPort);
        } catch(Exception e){
            e.printStackTrace();
        }
    }

    @Override
    public void run(){
        super.run();

        MsgSender.connect(MCsocket, MCAddress);

        Database db = Database.getInstance();

        while(true){
            byte[] info = new byte[MAX_PACKET_SIZE]; //nao deve ser necessário

            // Receber informacao
            String receivedS =  MsgReceiver.receive(MCsocket);

            // Get the message type
            String msgType = receivedS.split(" ")[0];

            if( msgType.equals("DELETE"))
            {
                FileDeletionReceiver fileDelRec = new FileDeletionReceiver(receivedS);
                fileDelRec.run();
            }
            else if( msgType.equals("GETCHUNK"))
            {
                RestoreReceiver restRec = new RestoreReceiver(receivedS, MDRAddress, MDRPort);
                restRec.run();
            }
            else if( msgType.equals("REMOVED"))
            {
                SpaceReclaimingReceiver spaceReclaimRec = new SpaceReclaimingReceiver(receivedS);
                spaceReclaimRec.run();
            }
            if( msgType.equals("EXIT"))
                break;
        }

        MsgSender.disconnect(MCsocket, MCAddress);

        MCsocket.close();
    }
}
