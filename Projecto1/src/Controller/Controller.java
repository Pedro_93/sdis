package Controller;

import java.io.File;
import java.net.URL;
import java.util.ArrayList;
import java.util.ResourceBundle;
import java.util.concurrent.ExecutorService;

import Client.Backup.BackupSender;
import Client.FileDeletion.FileDeletionSender;
import Client.Restore.RestoreSender;
import Client.SpaceReclaiming.SpaceReclaimingSender;
import Server.Database;
import application.Main;
import javafx.collections.FXCollections;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Accordion;
import javafx.scene.control.ListView;
import javafx.scene.control.TextField;
import javafx.scene.input.KeyEvent;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.GridPane;
import javafx.scene.text.Text;
import javafx.stage.FileChooser;


public class Controller implements Initializable{
	
	@FXML private TextField MCchannel1,MCchannel2,MCchannel3,MCchannel4,MDBchannel1,MDBchannel2,MDBchannel3,MDBchannel4,MDRchannel1,MDRchannel2,MDRchannel3,MDRchannel4,MCPort,MDBPort,MDRPort;
	
	@FXML
	private GridPane ConfigurationPanel;
	
	@FXML private Accordion ProtocolPanel;
	
	@FXML 
	private Text BackupFileName;
	@FXML
	private TextField RepDegField;
	@FXML
    private Text StatusMsg;

    @FXML
    private TextField RepDegree;

    @FXML
    private Text StatusField;

    @FXML
    private ListView RestFileList;

    @FXML
    private ListView SpaceFileList;

    @FXML
    private ListView DeleteFileList;

	final FileChooser fc = new FileChooser();
	private String FilePath;



	@Override
	public void initialize(URL location, ResourceBundle resources) {

		MCchannel1.addEventFilter(KeyEvent.KEY_TYPED, maxLength(3));
		MCchannel2.addEventFilter(KeyEvent.KEY_TYPED, maxLength(3));
		MCchannel3.addEventFilter(KeyEvent.KEY_TYPED, maxLength(3));
        MCchannel4.addEventFilter(KeyEvent.KEY_TYPED, maxLength(3));
        MCPort.addEventFilter(KeyEvent.KEY_TYPED, maxLength(7));

        MDBchannel1.addEventFilter(KeyEvent.KEY_TYPED, maxLength(3));
		MDBchannel2.addEventFilter(KeyEvent.KEY_TYPED, maxLength(3));
		MDBchannel3.addEventFilter(KeyEvent.KEY_TYPED, maxLength(3));
        MDBchannel4.addEventFilter(KeyEvent.KEY_TYPED, maxLength(3));
        MDBPort.addEventFilter(KeyEvent.KEY_TYPED, maxLength(7));


        MDRchannel1.addEventFilter(KeyEvent.KEY_TYPED, maxLength(3));
		MDRchannel2.addEventFilter(KeyEvent.KEY_TYPED, maxLength(3));
		MDRchannel3.addEventFilter(KeyEvent.KEY_TYPED, maxLength(3));
        MDRchannel4.addEventFilter(KeyEvent.KEY_TYPED, maxLength(3));
        MDRPort.addEventFilter(KeyEvent.KEY_TYPED, maxLength(7));

        MCchannel1.setText(Main.MCchannel.split("\\.")[0]);
        MCchannel2.setText(Main.MCchannel.split("\\.")[1]);
        MCchannel3.setText(Main.MCchannel.split("\\.")[2]);
        MCchannel4.setText(Main.MCchannel.split("\\.")[3]);
        MCPort.setText(Integer.toString(Main.MCport));

        MDBchannel1.setText(Main.MDBchannel.split("\\.")[0]);
        MDBchannel2.setText(Main.MDBchannel.split("\\.")[1]);
        MDBchannel3.setText(Main.MDBchannel.split("\\.")[2]);
        MDBchannel4.setText(Main.MDBchannel.split("\\.")[3]);
        MDBPort.setText(Integer.toString(Main.MDBport));

        MDRchannel1.setText(Main.MDRchannel.split("\\.")[0]);
        MDRchannel2.setText(Main.MDRchannel.split("\\.")[1]);
        MDRchannel3.setText(Main.MDRchannel.split("\\.")[2]);
        MDRchannel4.setText(Main.MDRchannel.split("\\.")[3]);
        MDRPort.setText(Integer.toString(Main.MDRport));



    }
	
	public void loadFile(ActionEvent e)
	{
		//Set to user directory or go to default if cannot access
		String userDirectoryString = System.getProperty("user.home");
		File userDirectory = new File(userDirectoryString);
		if(!userDirectory.canRead()) {
		    userDirectory = new File("c:/");
		}
		fc.setInitialDirectory(userDirectory);
		
		//Choose the file
		File chosenFile = fc.showOpenDialog(null);
		//Make sure a file was selected, if not return default
		if(chosenFile != null) {
			FilePath = chosenFile.getPath();
			BackupFileName.setText("filepath: "+FilePath);
		} else {
		    //default return value
			FilePath = null;
		}
	}
	
	public void runBackup(ActionEvent e)
	{
        if(RepDegree.getText().toString().matches("\\d+"))
        {
            try {
                BackupSender bckpsnd =  new BackupSender(FilePath,Integer.parseInt(RepDegree.getText().toString()),Main.MDBchannel,Main.MDBport,Main.MCchannel,Main.MCport);
                StatusField.setText("running, please wait");
                bckpsnd.run();
                StatusField.setText("done!");
            } catch (Exception e1) {
                e1.printStackTrace();
            }
        }
    }
	
	public void showConfigurationPage(ActionEvent e)
	{		
		ProtocolPanel.setVisible(false);
		ProtocolPanel.setDisable(true);
		ConfigurationPanel.setVisible(true);
		ConfigurationPanel.setDisable(false);
	}
	
	public void ShowProtocolPage(ActionEvent e)
	{
		ProtocolPanel.setVisible(true);
		ProtocolPanel.setDisable(false);
		ConfigurationPanel.setVisible(false);
		ConfigurationPanel.setDisable(true);
	}
	
	public void ResetConfigurations(ActionEvent e)
	{
        String aux1,aux2,aux3;
        if(MCchannel1.getText().toString().matches("\\d+") && MCchannel2.getText().toString().matches("\\d+") && MCchannel3.getText().toString().matches("\\d+") &&MCchannel3.getText().toString().matches("\\d+")
                && MDRchannel1.getText().toString().matches("\\d+") && MDRchannel2.getText().toString().matches("\\d+") && MDRchannel3.getText().toString().matches("\\d+") &&MDRchannel3.getText().toString().matches("\\d+")
                && MDBchannel1.getText().toString().matches("\\d+") && MDBchannel2.getText().toString().matches("\\d+") && MDBchannel3.getText().toString().matches("\\d+") &&MDBchannel3.getText().toString().matches("\\d+"))
        {
            aux1 = MCchannel1.getText().toString()+"."+ MCchannel2.getText().toString()+"."+MCchannel3.getText().toString()+"."+MCchannel4.getText().toString();
            aux2 = MDRchannel1.getText().toString()+"."+ MDRchannel2.getText().toString()+"."+MDRchannel3.getText().toString()+"."+MDRchannel4.getText().toString();
            aux3 = MDBchannel1.getText().toString()+"."+ MDBchannel2.getText().toString()+"."+MDBchannel3.getText().toString()+"."+MDBchannel4.getText().toString();
            if(MCPort.getText().toString().matches("\\d+") && MDRPort.getText().toString().matches("\\d+") && MDBPort.getText().toString().matches("\\d+"))
            {
                Main.MCchannel=aux1;
                Main.MDBchannel=aux2;
                Main.MDRchannel=aux3;
                Main.MCport=Integer.parseInt(MCPort.getText().toString());
                Main.MDRport=Integer.parseInt(MDRPort.getText().toString());
                Main.MDBport=Integer.parseInt(MDBPort.getText().toString());

                ShowProtocolPage(e);
            }
        }
        StatusMsg.setText("Invalid parameters");
    }

    public void updatedListViewRest(MouseEvent e)
    {
        ArrayList<String> lista= Database.getInstance().lookupFiles();
        //List<String> values = Arrays.asList("one", "two", "three");
        RestFileList.setItems(FXCollections.observableList(lista));
    }

    public void updatedListViewSpace(MouseEvent e)
    {
        ArrayList<String> lista= Database.getInstance().lookupFiles();
        //List<String> values = Arrays.asList("one", "two", "three");
        SpaceFileList.setItems(FXCollections.observableList(lista));
    }

    public void updatedListViewDel(MouseEvent e)
    {
        ArrayList<String> lista= Database.getInstance().lookupFiles();
        //List<String> values = Arrays.asList("one", "two", "three");
        DeleteFileList.setItems(FXCollections.observableList(lista));

    }

    public void runRestore(ActionEvent e)
    {
        try {
                String fileID = RestFileList.getSelectionModel().getSelectedItem().toString();
                RestoreSender restsnd = new RestoreSender("CHUNKS\\"+fileID,fileID.toCharArray(),Main.MCchannel,Main.MCport,Main.MDRchannel,Main.MDRport);
                StatusField.setText("running, please wait");
                restsnd.run();
                StatusField.setText("done!");
        } catch (Exception e1) {
            e1.printStackTrace();
        }
    }

    public void runSpaceReclaim(ActionEvent e)
    {
        try {
            String fileID = SpaceFileList.getSelectionModel().getSelectedItem().toString();
            int chunk = Integer.parseInt(fileID.substring(fileID.length() - 1));
            SpaceReclaimingSender spacereclaimsnd = new SpaceReclaimingSender(fileID.toCharArray(),"1.0".toCharArray(),chunk,Main.MCchannel,Main.MCport);
            StatusField.setText("running, please wait");
            spacereclaimsnd.run();
            StatusField.setText("done!");
        } catch (Exception e1) {
            e1.printStackTrace();
        }
    }

    public void runDelete(ActionEvent e)
    {
        try {
            String fileID = DeleteFileList.getSelectionModel().getSelectedItem().toString();
            FileDeletionSender filesnd = new FileDeletionSender(fileID.toCharArray(),Main.MCchannel,Main.MCport);
            StatusField.setText("running, please wait");
            filesnd.run();
            StatusField.setText("done!");
        } catch (Exception e1) {
            e1.printStackTrace();
        }
    }

	public EventHandler<KeyEvent> maxLength(final Integer i) {
        return new EventHandler<KeyEvent>() {
            @Override
            public void handle(KeyEvent arg0) {

                TextField tx = (TextField) arg0.getSource();
                if (tx.getText().length() >= i){
                    arg0.consume();
                }
            }
        };
    }
}

